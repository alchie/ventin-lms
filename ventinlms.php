<?php
/*
Plugin Name: Ventin LMS
Plugin URI: http://www.ventin.co
Description: Ventin LMS Plugin - Turn your WordPress site into a learning management system.
Version: 1.0
Author: Chester Alan
Author URI: http://www.chesteralan.com
*/

require('libs/lib.includes.php');
require('includes/vlms_main.php');
require('includes/vlms_course.php');
require('includes/vlms_module.php');
require('includes/vlms_lesson.php');
require('includes/vlms_quiz.php');
require('includes/vlms_tabs.php');

$vlmsCourse = new VentinLMSCourse;
$vlmsCourse->init();

$vlmsModule = new VentinLMSModule;
$vlmsModule->init();

$vlmsLesson = new VentinLMSLesson;
$vlmsLesson->init();

$vlmsQuiz = new VentinLMSQuiz;
$vlmsQuiz->init();

$vlmsTabs = new VentinLMSTabs;
$vlmsTabs->init();

$vlmsMain = new VentinLMSMain;
$vlmsMain->add_submenu("Courses", 'manage_options', "edit.php?post_type=ventinlms-course", "Ventin LMS - Course", 1);
$vlmsMain->init();
