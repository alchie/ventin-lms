<?php

if (! class_exists( 'Custom_Post_Type' )) {
 
class Custom_Post_Type 
    {
        protected $id;
        protected $name;
        protected $plural;
        protected $singular;
        protected $menu_name;
        protected $description;
        protected $public = true;
        protected $has_archive = true;
        protected $capability_type = 'page';
        protected $supports = array( 'title' ); // 'editor' , 'excerpt', 'thumbnail',  'comments', 'page-attributes' ); 'custom-fields', 'author', 'trackbacks', 'post-formats');
        protected $position = NULL;
		protected $hierarchical = false;
		protected $labels = array();
        protected $show_in_menu = true;
        protected $exclude_from_search = false;
        protected $publicly_queryable = true;
        protected $show_ui = true;
        protected $show_in_nav_menus = true;
        protected $show_in_admin_bar = true;
        protected $map_meta_cap = true;
        protected $can_export = true;
        protected $slug;
        protected $query_var;
        
        function __construct($id='', $name='', $plural='', $description = 'Description of Custom Post Type Here', $position=NULL) {
            $this->id = $id;
            $this->name = $name;
            $this->plural = $plural;
            $this->description = $description;
            $this->position = $position;
            $this->slug = $id;
            $this->query_var = $id;
        }    
        
        function init()
        {
			
            if( $this->id && $this->name ) {
                add_action( 'init', array(&$this, 'register_post_type' ) );
            }
            return $this;
        }
		
        function register_post_type()
        {
			
	         $args = array(
		        'labels' => $this->get_labels(),
		        'description' => __( $this->description ),
		        'public' => $this->public,
		        'has_archive' => $this->has_archive,
		        'rewrite' => array('slug' => $this->slug),
		        'menu_position' => $this->position,
		        'menu_icon' =>  plugins_url( 'images/' . $this->id . '-icon.png', dirname(__FILE__) ),
		        'capability_type'=>$this->capability_type,
		        'supports'=>$this->supports,
				'hierarchical'=>$this->hierarchical,
				'show_in_menu' => $this->show_in_menu,
				'show_in_nav_menus' => $this->show_in_nav_menus,
				'show_ui' => $this->show_ui,
				'can_export' => $this->can_export,
				'publicly_queryable'  => $this->publicly_queryable,
				'exclude_from_search' => $this->exclude_from_search,
				'query_var' => $this->query_var,
		        );
		    
	        register_post_type( $this->id , $args);
	        
	        
        }
        
		function get_labels() {
			$defaults = array(
				    'name' => __( $this->name ),
				    'singular_name' => __( $this->singular ),
				    'menu_name' => __( $this->menu_name ),
				    'all_items' => __('All ' . $this->plural ),
				    'add_new' => __('Add a ' . $this->name),
				    'add_new_item' => __('Add a ' . $this->name),
				    'edit_item' => __('Edit ' . $this->name),
				    'new_item' => __('New ' . $this->name),
				    'view_item' => __('View ' . $this->name),
				    'search_items' => __('Search ' . $this->plural),
				    'not_found' => __('No ' . $this->name . ' found'),
				    'not_found_in_trash' => __('No ' . $this->name . ' found in Trash'),
				    'parent_item_colon' => __('Parent ' . $this->name),
			    );
			
			return array_merge($defaults, $this->labels);
		}
		
		function set_label( $key, $value ) {
				$this->labels = array_merge($this->labels, array($key => $value));
				return $this;
		}
        
       function set_id($value) {
            $this->id = $value;
            return $this;
       }
       
       function set_name($value) {
            $this->name = ($value!='') ? $value : $this->plural;
            return $this;
       }
       
       function set_singular($value) {
            $this->singular = ($value!='') ? $value :  $this->name;
            return $this;
       }
       
       function set_plural($value) {
            $this->plural = $value;
            return $this;
       }
       
       function set_menu_name($value) {
            $this->menu_name = $value;
            if( $this->menu_name == '' ) {
				$this->menu_name = $this->plural;
			}
            return $this;
       }
       
       function set_description($value) {
            $this->description = $value;
            return $this;
       }  
       
       function set_public($value) {
            $this->description = (bool) $value;
            return $this;
       }  
       
       function set_has_archive($value) {
            $this->description = (bool) $value;
            return $this;
       }    
       
       function set_position($value) {
            $this->position = (int) $value;
            return $this;
       }   
       
       function set_capability_type($value) {
            $this->capability_type = $value;
            return $this;
       }   
       
       function set_supports($value) {
            $this->supports = $value;
            return $this;
       }
       
	   function set_hierarchical($value=false) {
		   $this->hierarchical = $value;
		   return $this;
	   }
	   
       function add_support($value) {
            $numargs = func_num_args();
            if ($numargs >= 2) {
                for ($i = 0; $i < $numargs; $i++) {
                    $this->supports[] = func_get_arg($i);
                }
            } else {
                $this->supports[] = $value;
            }
            return $this;
       }
       
       function show_in_menu($value=true) {
            $this->show_in_menu = $value;
            return $this;
       }  
        
       function exclude_from_search($value=false) {
            $this->exclude_from_search = $value;
            return $this;
       }
       
       function publicly_queryable($value=true) {
            $this->publicly_queryable = $value;
            return $this;
       } 
       
       function show_ui($value=true) {
            $this->show_ui = $value;
            return $this;
       } 
       
       function show_in_nav_menus($value=true) {
            $this->show_in_nav_menus = $value;
            return $this;
       } 
       
       function show_in_admin_bar($value=true) {
            $this->show_in_admin_bar = $value;
            return $this;
       } 
       
       function map_meta_cap($value=true) {
            $this->map_meta_cap = $value;
            return $this;
       } 
       
       function can_export($value=true) {
            $this->can_export = $value;
            return $this;
       } 
       
       function set_slug($value) {
            $this->slug = $value;
            return $this;
       }
       
    }
}
