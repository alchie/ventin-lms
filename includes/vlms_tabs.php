<?php 

if ( !class_exists('VentinLMSTabs') )
{
	class VentinLMSTabs {

        function init() {
			add_action('all_admin_notices', array($this, 'generate_tabs'));
        }
        
        function generate_tabs() {
            $screen = get_current_screen();
            //print_r( $screen );
            if( $screen->base == 'post' && $screen->post_type == 'course' && $screen->parent_base == 'edit' && $screen->action == '' ) {
				echo 'course child tabs here';
			}
        }
        
    }
}
