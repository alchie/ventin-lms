<?php 

if ( !class_exists('VentinLMSMain') )
{
	class VentinLMSMain {
        var $menus = array();
        
        function init() {
			add_action( 'admin_menu', array($this, 'admin_menu') );
			add_action( 'admin_enqueue_scripts', array($this, 'load_admin_style') );
			add_action( 'all_admin_notices', array($this,'breadcrumbs') );
        }
        
        function admin_menu() {
			
			$slug = 'ventinlms';
			add_menu_page( "Ventin LMS", "Ventin LMS", 'manage_options', $slug, array($this, 'main_page'), plugins_url( 'ventinlms/images/course-icon.png' ), 1000 );
			add_submenu_page( $slug, "Ventin LMS - Settings", "Settings", 'manage_options', $slug . "-settings", array($this, 'settings_page') );
			
			global $submenu;
			unset($submenu[$slug][0]);
			$this->menus[999] = $submenu[$slug][1];
			
			$submenu[$slug] = $this->menus;

		}
        
        function breadcrumbs() {
		$screen = get_current_screen();
		
if( $screen->base == 'toplevel_page_ventinlms' ) {
echo '<div class="ventin-breadcrumb flat">
	<a href="admin.php?page=ventinlms">Ventin LMS</a>
	<a href="#" class="active">Dashboard</a>
</div>';
} elseif( $screen->base == 'ventin-lms_page_ventinlms-settings' ) {
echo '<div class="ventin-breadcrumb flat">
	<a href="admin.php?page=ventinlms">Ventin LMS</a>
	<a href="#" class="active">Settings</a>
</div>';
}
		}
        
        function main_page() {
echo "<div class='wrap'>";

echo "</div>";
		} 
		
		function settings_page() {
echo "<div class='wrap'>";

echo "</div>";
		} 
		
		function add_submenu($menu_name, $capability, $url, $page_title, $position) {
			$this->menus[$position] = array(
				0 => $menu_name,
				1 => $capability,
				2 => $url, 
				3 => $page_title
			);
		}
		
		function load_admin_style() {
			$screen = get_current_screen();
			wp_register_style( 'ventin_styles', plugins_url('/ventinlms/assets/css/ventin.css'), false, '1.0.0' );
			
			if(  strstr( $screen->post_type, 'ventinlms' ) != '' ) { 
				wp_enqueue_style( 'ventin_styles' );
			}
			if(  strstr( $screen->base, 'ventinlms' ) != '' ) { 
				wp_enqueue_style( 'ventin_styles' );
			}
		}
    }
}
