<?php 

if ( !class_exists('VentinLMSQuiz') )
{
	class VentinLMSQuiz {
        private $post_type_id = 'ventinlms-quiz';
        private $postType;
		private $course;
		private $module;
		private $lesson;
		
        function init() {
			$this->post_type_init();
			add_action( 'current_screen', array($this,'metabox_init') );
			add_action( 'current_screen', array($this,'check_requirement') );
			add_action( 'all_admin_notices', array($this,'tabs') );
			add_action( "load-{$GLOBALS['pagenow']}", array($this,'help') );
			add_filter('manage_'.$this->post_type_id.'_posts_columns', array($this,'posts_columns'));
			add_filter('page_row_actions', array($this,'row_actions'), 10, 2);
			add_filter('post_row_actions', array($this,'row_actions'), 10, 2);
			add_filter('bulk_actions-edit-' . $this->post_type_id, array($this,'bulk_actions'));
			add_action( 'save_post', array($this,'save_post') );
			add_filter( "views_edit-" . $this->post_type_id, array($this,'subsubsub') );
			add_filter( "redirect_post_location", array($this,'redirect_post_location'), 10, 2 );
			add_action( 'pre_get_posts', array($this,'pre_get_posts') ); 
        }
        
        function post_type_init() {
            if ( class_exists('Custom_Post_Type') ) {
				$this->course = get_post( $_GET['course_id'] );
				$this->module = get_post( $_GET['module_id'] );
				$this->lesson = get_post( $_GET['lesson_id'] );
                $this->postType = new Custom_Post_Type;                
                $this->postType->set_id( $this->post_type_id )
					->set_name('Quiz')
					->set_singular( 'Quiz')
					->set_plural( 'Quizzes')
					->set_position(1003)
					->add_support('editor')
					->add_support('page-attributes')
					->add_support('thumbnail')
					//->add_support('excerpt')
					//->set_hierarchical(true)
					->set_label('add_new', 'Add New Quiz')
					->set_label('add_new_item', 'Add New Quiz')
					->show_in_menu(false)
					->exclude_from_search(true)
					->set_slug('quiz')
					->init();
            }
        }
        
        function check_requirement() {
			$redirect = false;
			$check_requirement = false;
			$screen = get_current_screen();
			$url = 'edit.php?post_type=ventinlms-course';

			if( $screen->post_type == $this->post_type_id ) {
				
				if( $screen->base == 'edit' ) {
					$check_requirement = true;
				}
				if( $screen->action == 'add' && $screen->base == 'post' ) {
					$check_requirement = true;
				}
				if( $_GET['action'] == 'edit' && $screen->base == 'post' ) {
					$check_requirement = true;
				}
				if( $check_requirement ) {
					
					if( (!isset($_GET['course_id'])) || ($_GET['course_id'] == '') ) {
						$redirect = true;	
					}
					if( $this->course->post_type != 'ventinlms-course' ) {
						$redirect = true;
					}
					if( $this->module->post_type != 'ventinlms-module' ) {
						$redirect = true;
					}
					if( $this->lesson->post_type != 'ventinlms-lesson' ) {
						$redirect = true;
					}
					
					$course_id = get_post_meta( $this->course->ID, '_ventinlms_course_id', true);
					
				}
				if( $redirect ) {
					header('location: ' . $url);
					exit;
				}
			}
			
		}
		
		function tabs() {
			$screen = get_current_screen();
			
			if( $screen->post_type == $this->post_type_id ) {
				
				$tabs = array(
					array(
					'link' => 'post-new.php?post_type=' . $this->post_type_id . "&course_id=" . $this->course->ID . "&module_id=" . $this->module->ID . "&lesson_id=" . $this->lesson->ID,
					'name' => 'Add New Quiz',
					),
					
					array(
					'link' => 'edit.php?post_type=' . $this->post_type_id . "&course_id=" . $this->course->ID . "&module_id=" . $this->module->ID . "&lesson_id=" . $this->lesson->ID,
					'name' => 'Quizzes',
					),
					
				);
					$active_tab = NULL;
					if( $screen->base == 'post' && $screen->action == 'add' ) {
						$active_tab = 0;
					}
					if( $screen->base == 'edit' && $screen->id == 'edit-ventinlms-quiz' ) {
						$active_tab = 1;
					}
					
echo '<div class="ventin-breadcrumb flat">
	<a href="admin.php?page=ventinlms">Ventin LMS</a>
	<a href="post.php?post=' . $this->course->ID . '&action=edit">Courses</a>
	<a href="post.php?post=' . $this->module->ID . '&action=edit&course_id=' . $this->course->ID . '">Modules</a>
	<a href="post.php?post=' . $this->lesson->ID . '&action=edit&course_id=' . $this->course->ID . '&module_id=' . $this->module->ID . '">Lessons</a>
	<a href="#" class="active">Quizzes</a>
</div>';
					echo '<h2 class="nav-tab-wrapper">';
						foreach( $tabs as $key=>$tab ) {
							$active_class = ($key === $active_tab) ? 'nav-tab-active' : '';
							echo '<a href="'.$tab['link'].'" class="nav-tab nav-tab-'.$this->post_type_id.' '.$active_class.'">'.$tab['name'].'</a>';
						}
					echo '</h2>';
					
				
			
			}
		}
		
		function help() {
			$screen = get_current_screen();
			
			if( $screen->post_type == $this->post_type_id ) {
				 $screen->add_help_tab( array(
					'id'	=> 'add_new_course',
					'title'	=> __('Add New Course'),
					'content'	=> '<p>' . __( 'Descriptive content that will show in My Help Tab-body goes here.' ) . '</p>',
				) );
			}
			
		}   
		
		function posts_columns($defaults) {
			unset( $defaults['date'] );
			return $defaults;
		}
        
        function row_actions( $actions, $post ) {
			$screen = get_current_screen();
			$post_status = ( !isset($_GET['post_status']) || $_GET['post_status'] !== 'trash') ? true : false;
			if( ($screen->post_type == $this->post_type_id) && $post_status ) {
				$edit_link = get_edit_post_link( $post->ID );
				$actions['edit'] = '<a href="'.$edit_link.'&amp;course_id='.$this->course->ID.'&amp;module_id='.$this->module->ID.'&amp;lesson_id='.$this->lesson->ID.'" title="Edit this module">Edit Quiz</a>';
				
				$preview_link = set_url_scheme( get_permalink( $post->ID ) );
				$actions['view'] = '<a href="' . esc_url( $preview_link ) . '" target="_blank" title="' . esc_attr( sprintf( __( 'Preview &#8220;%s&#8221;' ), $title ) ) . '" rel="permalink">' . __( 'View Quiz' ) . '</a>';
				
			}
			return $actions;
		}
		
		function bulk_actions( $actions ) {
			echo "<input type='hidden' name='course_id' value='{$this->course->ID}'>";
			echo "<input type='hidden' name='module_id' value='{$this->module->ID}'>";
			echo "<input type='hidden' name='lesson_id' value='{$this->lesson->ID}'>";
			return $actions;
		}
		
		function save_post( $post_id ) {
			$screen = get_current_screen();
			if( $screen->action=='add' && $screen->base=='post' && $screen->post_type == $this->post_type_id ) {
				update_post_meta( $post_id, '_ventinlms_course_id', $this->course->ID );
				update_post_meta( $post_id, '_ventinlms_module_id', $this->module->ID );
				update_post_meta( $post_id, '_ventinlms_lesson_id', $this->lesson->ID );
			}
		}
		
		function subsubsub( $views ) {
			$views2 = array();
			$screen = get_current_screen();
			if( $screen->post_type == $this->post_type_id ) {
				foreach( $views as $key=> $view) {
					$views2[$key] = str_replace($this->post_type_id, $this->post_type_id.'&amp;course_id='.$this->course->ID.'&amp;module_id='.$this->module->ID.'&amp;lesson_id='.$this->lesson->ID, $view);
				}
				return $views2;
			} else {
				return $views;
			}
		}
		
		function redirect_post_location( $location, $post_id ) {
			$screen = get_current_screen();
			if( $screen->post_type == $this->post_type_id ) {
				$location = $location . "&course_id=" . $_POST['_ventinlms_course_id'] . "&module_id=" . $_POST['_ventinlms_module_id'] . "&lesson_id=" . $_POST['_ventinlms_lesson_id'];
			}
			return $location;
		}
		
		function pre_get_posts( $query ) {
			if( ! is_admin() ) return;
			$screen = get_current_screen();
			if( $screen->post_type == $this->post_type_id ) {
				$meta_query_args = array(
						array(
							'key'     => '_ventinlms_course_id',
							'value'   => $this->course->ID,
							'compare' => '='
						),
						array(
							'key'     => '_ventinlms_module_id',
							'value'   => $this->module->ID,
							'compare' => '='
						),
						array(
							'key'     => '_ventinlms_lesson_id',
							'value'   => $this->lesson->ID,
							'compare' => '='
						)
					);
				$query->set( 'meta_query', $meta_query_args );
			}
		}
		
		function metabox_init() {
			if ( class_exists('Custom_Metabox') ) { 
					$metabox = new Custom_Metabox( $this->post_type_id , 'ventinlms_assignment_details', 'Course, Module &amp; Lesson', 'side', 'high');
					$metabox->add_field( array('type' => 'desc','desc' => '<strong>Course: </strong>' . $this->course->post_title ) );
					$metabox->add_field( array('type' => 'hidden','default' => $this->course->ID, 'id'=> '_ventinlms_course_id' ) );
					$metabox->add_field( array('type' => 'desc','desc' => '<strong>Module: </strong>' . $this->module->post_title ) );
					$metabox->add_field( array('type' => 'hidden','default' => $this->module->ID, 'id'=> '_ventinlms_module_id' ) );
					$metabox->add_field( array('type' => 'desc','desc' => '<strong>Lesson: </strong>' . $this->lesson->post_title ) );
					$metabox->add_field( array('type' => 'hidden','default' => $this->lesson->ID, 'id'=> '_ventinlms_lesson_id' ) );
					$metabox->init();
                }
        }
    }
}
