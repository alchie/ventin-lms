<?php 

if ( !class_exists('VentinLMSCourse') )
{
	class VentinLMSCourse {
        private $post_type_id = 'ventinlms-course';

        function init() {
			$this->post_type_init();
			add_action( 'all_admin_notices', array($this,'tabs') );
			add_action( "load-{$GLOBALS['pagenow']}", array($this,'help') );
			add_filter('manage_ventinlms-course_posts_columns', array($this,'posts_columns'));
			add_filter('page_row_actions', array($this,'row_actions'), 10, 2);
			add_filter('post_row_actions', array($this,'row_actions'), 10, 2);
        }
        
        function post_type_init() {
            if ( class_exists('Custom_Post_Type') ) {
                $this->employee = new Custom_Post_Type;                
                $this->employee->set_id( $this->post_type_id )
					->set_name('Course')
					->set_plural('Courses')
					->set_menu_name('Courses')
					->set_position(1000)
					->add_support('editor')
					->add_support('thumbnail')
					//->add_support('excerpt')
					//->set_hierarchical(true)
					->set_label('add_new', 'Add New Course')
					->set_label('add_new_item', 'Add New Course')
					->show_in_menu(false)
					->set_slug('course')
					->init(); 
            }
        }
        
        function tabs() {
			$screen = get_current_screen();
			
			if( $screen->post_type == $this->post_type_id ) {
				
				$tabs = array(
					array(
					'link' => 'post-new.php?post_type=' . $this->post_type_id,
					'name' => 'Add New Course',
					),
					array(
					'link' => 'edit.php?post_type=' . $this->post_type_id,
					'name' => 'Courses',
					),
				);
					$active_tab = NULL;
					if( $screen->base == 'post' && $screen->action == 'add' ) {
						$active_tab = 0;
					}
					if( $screen->base == 'edit' && $screen->id == 'edit-ventinlms-course' ) {
						$active_tab = 1;
					}
					if( $screen->action == '' && $screen->base == 'post' && $screen->parent_base == 'edit' && $screen->parent_file == 'edit.php?post_type=ventinlms-course' ) {
						$tabs[] = array(
								'link' => 'edit.php?post_type=ventinlms-module&course_id=' . $_GET[ 'post' ],
								'name' => 'Modules',
								);
					}
					
echo '<div class="ventin-breadcrumb flat">
	<a href="admin.php?page=ventinlms" >Ventin LMS</a>
	<a href="#" class="active">Courses</a>
</div>';	
					echo '<h2 class="nav-tab-wrapper">';
						foreach( $tabs as $key=>$tab ) {
							$active_class = ($key === $active_tab) ? 'nav-tab-active' : '';
							echo '<a href="'.$tab['link'].'" class="nav-tab nav-tab-'.$this->post_type_id.' '.$active_class.'">'.$tab['name'].'</a>';
						}
					echo '</h2>';	
		
			
			}
		}
		
		function help() {
			$screen = get_current_screen();
			if( $screen->post_type == $this->post_type_id ) {
				 $screen->add_help_tab( array(
					'id'	=> 'add_new_course',
					'title'	=> __('Add New Course'),
					'content'	=> '<p>' . __( 'Descriptive content that will show in My Help Tab-body goes here.' ) . '</p>',
				) );
			}
			
		}  
		
		function posts_columns($defaults) {
			unset( $defaults['date'] );
			return $defaults;
		}  
		
		function row_actions( $actions, $post ) {
			$screen = get_current_screen();
			$post_status = ( !isset($_GET['post_status']) || $_GET['post_status'] !== 'trash') ? true : false;
			if( ($screen->post_type == $this->post_type_id ) && $post_status ) {
				$edit_link = get_edit_post_link( $post->ID );
				$actions['edit'] = '<a href="'.$edit_link.'" title="Edit this course">Edit Course</a>';
				
				$preview_link = set_url_scheme( get_permalink( $post->ID ) );
				$actions['view'] = '<a href="' . esc_url( $preview_link ) . '" target="_blank" title="' . esc_attr( sprintf( __( 'Preview &#8220;%s&#8221;' ), $title ) ) . '" rel="permalink">' . __( 'View Course' ) . '</a>';
				
				$actions['course_modules'] = '<a href="edit.php?post_type=ventinlms-module&amp;course_id='.$post->ID.'" title="Course Modules">Modules</a>';
				$actions['add_module'] = '<a href="post-new.php?post_type=ventinlms-module&amp;course_id='.$post->ID.'" title="Add New Module">Add New Module</a>';
			}
			return $actions;
		}   
    }
}
